#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include "DHT.h"
#include <math.h>
#include <SPI.h>
#include <Ethernet.h>

/*temperature,huminidity sensor define*/
#define DHTPIN 2     // what digital pin we're connected to
#define DHTTYPE DHT21   // DHT 21
DHT dht(DHTPIN, DHTTYPE); // DHT 21


/*LCD I2C define*/
#define I2C_ADDR    0x3F // <<----- Add your address here.  Find it from I2C Scanner
#define BACKLIGHT_PIN     3 //
#define En_pin  2 // LCD
#define Rw_pin  1 // LCD
#define Rs_pin  0 // LCD
#define D4_pin  4 // LCD
#define D5_pin  5 // LCD
#define D6_pin  6 // LCD
#define D7_pin  7  //LCD

int n = 1; // LCD
LiquidCrystal_I2C  lcd(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin); // LCD


/*O2 sensor define*/
float VoutArray[] =  { 0.0000141 ,0.2096004, 0.4191945 ,0.6342458 ,1.275509 ,1.968973};
float  O2ConArray[] =  { 0.00018, 2.66129, 5.32258, 8.05300, 16.19851, 25.00000};
const int buzzerPin = 3;  // O2 
float thresholdVoltage = 1.84;  // O2


/*Ethernet define*/
// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 177);

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only

  }

  // start the Ethernet connection and the server:
  Ethernet.begin(mac, ip);
  server.begin();
  Serial.print("server is at ");
  Serial.println(Ethernet.localIP());

  lcd.begin (16,2);
  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
  lcd.setBacklight(HIGH);
  lcd.home ();

  dht.begin();
}


void loop() {

  delay(5000);  // Delay 5sec

  /*tem,humi*/
  float huminity = dht.readHumidity();
  float tempC = dht.readTemperature();
  float f = dht.readTemperature(true);

  float hif = dht.computeHeatIndex(f, huminity);
  float hic = dht.computeHeatIndex(tempC, huminity, false);

  // Update Display
  lcd.backlight();
  lcd.print("T:");
  lcd.print(tempC);
  lcd.setCursor(0,1);
  lcd.print("H:");
  lcd.print(huminity);
  lcd.print("  O:");
  lcd.print(readConcentration(A0));

  // listen for incoming clients
  EthernetClient client = server.available();
  if (client) {
    Serial.println("new client");

    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply

        if (c == '\n') {
          Serial.println("Sent to client");
          client.print(tempC);
          client.print(",");
          client.print(huminity);
          client.print(",");
          client.print(readO2Vout(A0));
          client.print(",");
          client.println(readConcentration(A0));
          break;
        }
      }
    }
    client.stop();
    Serial.println("client disconnected");
  }
}



float readO2Vout(uint8_t analogpin)
{
    // Vout samples are with reference to 3.3V
    float MeasuredVout = analogRead(A0) * (3.3 / 1023);
    return MeasuredVout;

}

float readConcentration(uint8_t analogpin)
{


    // Vout samples are with reference to 3.3V
    float MeasuredVout = analogRead(A0) * (3.3 / 1023);
    float Concentration = FmultiMap(MeasuredVout, VoutArray,O2ConArray, 6);
    float Concentration_Percentage=Concentration;

    /**************************************************************************

    The O2 Concentration in percentage is calculated based on wiki page Graph


    The data from the graph is extracted using WebPlotDigitizer
    http://arohatgi.info/WebPlotDigitizer/app/

    VoutArray[] and O2ConArray[] are these extracted data. Using MultiMap, the data
    is interpolated to get the O2 Concentration in percentage.

    This implementation uses floating point arithmetic and hence will consume
    more flash, RAM and time.

    The O2 Concentration in percentage is an approximation and depends on the accuracy of
    Graph used.

    ***************************************************************************/

    return Concentration_Percentage;
}


//This code uses MultiMap implementation from http://playground.arduino.cc/Main/MultiMap

float FmultiMap(float val, float * _in, float * _out, uint8_t size)
{
  // take care the value is within range
  // val = constrain(val, _in[0], _in[size-1]);
  if (val <= _in[0]) return _out[0];
  if (val >= _in[size-1]) return _out[size-1];

  // search right interval
  uint8_t pos = 1;  // _in[0] allready tested
  while(val > _in[pos]) pos++;

  // this will handle all exact "points" in the _in array
  if (val == _in[pos]) return _out[pos];

  // interpolate in the right segment for the rest
  return (val - _in[pos-1]) * (_out[pos] - _out[pos-1]) / (_in[pos] - _in[pos-1]) + _out[pos-1];
}
