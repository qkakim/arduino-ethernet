﻿import socket
import sys

server_ip = '192.168.1.177'
server_port = 80

# Create a TCP/IP socket
print('Connect to {}:{}'.format(server_ip, server_port))

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((server_ip, server_port))
sock.send(b'Hello\n')
data = []
buf = sock.recv(256)
while len(buf) > 0:
    data.append(buf)
    buf = sock.recv(256)
sock.close()

data = ''.join( d.decode('utf-8') for d in data )
temp, hum, vol, con = [ float(d) for d in data.split(',') ]

print("Temparature: {}".format(temp))
print("humidity: {}".format(hum))
print("Vout: {}".format(vol))
print("o2concnetration: {}".format(con))
